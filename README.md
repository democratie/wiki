# :octopus: https://democratie.gitlab.io/wiki :swimmer:

Sources du wiki sur la démocratie 💧 liquide 💧.

---

### [Nos chantiers](pages/20.projects/default.fr.md) :

1. [__App__](pages/20.projects/10.app/default.fr.md)
2. [__Mairies__](pages/20.projects/20.call-for-action/readme.fr.md)
3. __Contagion__ culturelle
4. __Lobbying__ avec les collectifs de civictech  [DémocratieOuverte](https://democratieouverte.org/) et [CodeForFrance](https://codefor.fr)



### Comment contribuer

Voir [le guide](https://democratie.gitlab.io/wiki/fr/a-propos/contribuer).