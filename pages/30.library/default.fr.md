---
title: Bibliothèque
slug: bibliotheque
---

Collectons ici la _littérature existante_ au sujet de la _démocratie liquide_.

> N'oubliez pas de ramener les livres au plus trente jours après les avoir empruntés.


## Démocratie Liquide


### Papiers

- 🇬🇧 [Ford, Bryan – Delegative Democracy](http://bford.info/deleg/)

!!! [PLUS 🔬🔬🔬](éditez)


### Revues

- [Démocratie Liquide — Ianik Marcil](https://www.cairn.info/revue-multitudes-2012-3-page-210.htm)
- [_chhercher sur cairn.info_](https://www.cairn.info/resultats_recherche.php?searchTerm=%22d%C3%A9mocratie+liquide%22)


### Livres

- [Le Coup d'État Citoyen](https://www.editionsladecouverte.fr/catalogue/index-Le_coup_d___tat_citoyen-9782707192431.html)
- […](éditer)

<marquee><span style="cursor: progress;" onclick="alert('🔎📖🍀?')">🦀</span></marquee>


## Corollaires

Ce qui n'est pas directement au sujet de la démocratie liquide, mais qui la concerne.


### Papiers

- [Rapport sur l’« Initiative législative en Europe »](https://www.venice.coe.int/webforms/documents/default.aspx?pdffile=CDL-AD(2008)035-f), 2008.
- 🇬🇧 [The Sybil Attack](papers/Douceur,%20John%20R.%20-%20The%20Sybil%20Attack.pdf)
  > This paper shows that, without a logically centralized authority,
  > Sybil attacks are always possible
  > except under extreme and unrealistic assumptions
  > of resource parity and coordination among entities.
- 🇬🇧 [Web of Trust](https://www.weboftrust.info/papers.html)


## Sur ce wiki

- Quelques chiffres [au sujet de l'assemblée nationale](studies/assemblee-nationale-etude-des-deputes)


## Voir aussi

- [Wikipédia](https://fr.wikipedia.org/wiki/D%C3%A9mocratie_liquide)
- [Parti Pirate](https://discourse.partipirate.org/search?q=liquide)
- [CommunityWiki](https://communitywiki.org/wiki/D%C3%A9mocratieLiquide)
- 🇬🇧 [P2P-foundation wiki](https://wiki.p2pfoundation.net/Liquid_Democracy)
- 🇬🇧 [Campaigns Wikia](https://campaigns.fandom.com/wiki/Liquid_Democracy)
- 🇪🇸 [Democracia liquida](http://www.democracialiquida.org/)
- [Ajoutez vos liens](éditer) !


<figure markdown="1">
  <img src="https://wiki.piratenpartei.de/wiki/images/b/bf/Liquid_Democracy_Prozess.gif"
       alt="Animation montrant les flux de votes dans une démocratie liquide." />
  <figcaption>
    Voter dans une démocratie liquide, un exemple.<br>
    Courtoisie du [_Parti Pirate_](https://partipirate.org/).
  </figcaption>
</figure>
