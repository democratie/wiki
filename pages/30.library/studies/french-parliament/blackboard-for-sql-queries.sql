SELECT v.deputyId, d.firstname, d.lastname, COUNT(*) AS count_votes
FROM `vote` AS v, `deputy` AS d
WHERE
  v.value="for"
  AND
  v.deputyId = d.officialId
GROUP BY deputyId
ORDER BY `count_votes` DESC;



SELECT
	d.firstname,
    d.lastname,
    CONCAT(d.firstname, ' ', d.lastname) as fullname
FROM
	`deputy` as d;


SELECT
    d.firstname,
    d.lastname,
    CONCAT(d.firstname, ' ', d.lastname) AS fullname,
    (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId) AS votes_total,
    (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId AND v.value = 'for') AS votes_yay,
    (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId AND v.value = 'against') AS votes_nay
FROM
    `deputy` as d
ORDER BY votes_total DESC;


--vaote.value=
--  for
--  blank
--  non-voting
--  against



SELECT
    d.firstname,
    d.lastname,
    CONCAT(d.firstname, ' ', d.lastname) AS fullname,
    d.email,
    d.votes_total,
    d.votes_yay,
    d.votes_nay,
    d.votes_blk,
    d.abstentions,
    d.votes_yay / d.votes_nay AS enthusiasm
FROM (
    SELECT
      d.*,
      (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId) AS votes_total,
      (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId AND v.value = 'for') AS votes_yay,
      (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId AND v.value = 'against') AS votes_nay,
      (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId AND v.value = 'blank') AS votes_blk,
      (SELECT COUNT(*) FROM `vote` AS v WHERE v.deputyId = d.officialId AND v.value = 'non-voting') AS abstentions
    FROM
      `deputy` as d
) AS d

ORDER BY enthusiasm DESC;

