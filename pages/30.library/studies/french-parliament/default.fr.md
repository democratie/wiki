---
title: "Assemblée Nationale 👄 Les Député⋅es"
slug: assemblee-nationale-etude-des-deputes
---

## Sources

### Direct Assemblée

Un dump au 2019-04 avec 577 député⋅es, près de 180000 votes et 25000 travaux.
Disponible sur [IPFS](https://ipfs.io/ipfs/QmUm9qKMP8DyEW6qtrRsEHdfbsdK4HTF3vo64FyWc2Xt7d).

Quelques [exemples de requêtes SQL](blackboard-for-sql-queries.sql) à effectuer sur cette base…


### [NosDeputes.fr](https://nosdeputes.fr)

Ils sont [tous là](https://www.nosdeputes.fr/deputes) !

Le [CSV](french-deputies-2019-04.csv) dérivé qui a servi
à faire ces SVGs avec [rawgraphs.io](https://rawghraphs.io).


## Études

### Enthousiaste ou réfractaire ?

En éliminant les abstentions et les votes blancs, on obtient le paysage suivant :

#### Par groupe

![Enthousiasme par groupe](enthusiasm-per-group-2019-04.svg){.center}

#### Par député⋅e

![Proportion des votes Pour ou Contre pour chaque député⋅e au 2019-04](french-deputies-yay-nay-2019-04.fr.svg){.center}


