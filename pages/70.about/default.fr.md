---
title: À Propos
slug: a-propos
---



Ce wiki peut être considéré comme un **atelier constituant vivant** au sujet de la **démocratie liquide**.



### Discuter {#discuter}

!!! Rejoignez-nous sur les réseaux :

<div class="tiles-reel" markdown="1">

[![Email](logos/mail-tile.svg)](mailto:bonjour@assemblee-liquide.fr){.tile .rubber .discord}
[![Discord](logos/discord-tile.svg)](https://discord.gg/qnb9pCM){.tile .rubber .discord}
[![Facebook](logos/facebook-tile.svg)](https://www.facebook.com/groups/assemblee.liquide){.tile .rubber .facebook}
[![Twitter](logos/twitter-tile.svg)](https://twitter.com/voteliquide){.tile .rubber .twitter}
[![GitLab](logos/gitlab-tile.svg)](https://gitlab.com/democratie/wiki/issues){.tile .rubber .gitlab}
[![Telegram](logos/telegram-tile.svg)](https://t.me/joinchat/KnRbbhUKmXsoSE_FuUSMmg){.tile .rubber .riot}
[![ActivityPub](logos/activitypub-tile.svg)](https://reseaujaune.com/@assemblee){.tile .rubber .activitypub}
[![Riot](logos/riot-tile.svg)](https://riot.im/app/#/room/#ric:matrix.org){.tile .rubber .riot}

</div>

Vous avez repéré un bogue ou vous avez une idée pour une nouvelle fonctionnalité ?
[Ouvrez un ticket !](https://gitlab.com/democratie/wiki/issues)



### Contribuer {#contribuer}

Créez votre compte sur [GitLab 🦊](https://gitlab.com/democratie) pour pouvoir contribuer.

C'est gracieusement gratuit pour les projets libres comme les nôtres,
et cela nous met à l'abri des bots spammeurs.

[En savoir plus…](./contribuer)
