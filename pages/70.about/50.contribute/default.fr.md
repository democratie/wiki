---
title: "Contribuer"
slug: contribuer
---


![Puzzle pieces of gold and frozen tears](puzzle-gold-and-tears.png){.center}

## Bienvenue, pièce du puzzle {.center}


### Comment ça marche ?

Les sources de ce wiki sont journalisées dans un [dépôt Git](https://gitlab.com/democratie/wiki).

! Chaque _page_ est un _dossier_, qui contient au moins un fichier `.md` (Markdown),
! avec le contenu de la page ainsi que ses métadonnées localisées.

Les fichiers suivent les conventions de [Grav](https://learn.getgrav.org/15/content/content-pages#folders).



### Editer une page {#editer-une-page}

Cliquez sur `</>` en pied de page pour aller directement à la source de celle-ci.

Toutes les sources sont [sur GitLab](https://gitlab.com/democratie).



### Créer une page {#creer-une-page}

Assurez-vous d'utiliser des _slugs_ pour les noms de dossiers, sans diacritiques.
Exemple de slug: `40.noel-ou-ce-zephyr-hai`.

<marquee title="slug-cased">🐌-🐌-🐌</marquee>
<marquee title="snake_cased" direction="right">🐍\_🐍\_🐍</marquee>

> Les préfixes numéraires sont éliminés des URLs et ne servent que pour ordonner la navigation.

!!!! Évitez les _diacritiques_ (accents et autres) dans les _slugs_.



### Règlement intérieur

- Slugs 🐌 _(de préférence en anglais)_
- Faites comme bon vous semble, dans le respect.
- N'hésitez pas à ajouter du contenu partiel, imparfait, qui appelle à l'aide ;
_ne laissez pas votre atélophobie vous immobiliser !_

!! Les sauts de ligne et les espaces sont gratuits et respectent
!! les personnes qui n'ont pas une aussi bonne vue que vous,
!! quand elles doivent elles aussi lire la source.
