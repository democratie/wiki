---
title: About
slug: about
---

This wiki is (initially) a french workbench for _liquid democracy_.

!! You can totally use it in your own language if you want.
!! Need [help](https://gitlab.com/democratie/wiki/issues)?

There's also the [Federated Wiki about Liquid Democracy](http://liquiddemocracy.org).
(their [HTTPS](https://liquiddemocracy.org) looks dead at the time)



### Discuss {#discuss}

Found a bug?  Want a feature?
[Open a ticket!](https://gitlab.com/democratie/wiki/issues)


<div class="tiles-reel" markdown="1">

[![Email](logos/mail-tile.svg)](mailto:bonjour@assemblee-liquide.fr){.tile .rubber .discord}
[![Discord](logos/discord-tile.svg)](https://discord.gg/ka9hDr){.tile .rubber .discord}
[![GitLab](logos/gitlab-tile.svg)](https://gitlab.com/democratie/wiki/issues){.tile .rubber .gitlab}

</div>

! Add what [you have](edit)!

