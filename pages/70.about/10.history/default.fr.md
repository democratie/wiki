---
title: Histoire
slug: histoire
---

## Histoire de la démocratie liquide

[…](éditer)


### Chronométrie de la littérature scientifique

Nombre de _papiers_ (et _livres_) publiés contenant `"liquid democracy"`,
d'après [Google Scholar](https://scholar.google.fr/scholar?q=%22liquid+democracy%22&hl=en&as_sdt=0%2C5&as_vis=1).
Télécharger le [CSV](amounts-of-papers-and-books-about-liquid-democracy.csv).

![Série temporelle de la quantité totale de papiers mentionnant la démocratie liquide](serie-temporelle-democratie-liquide-papiers.fr.svg){.center}

<marquee>
<style>
/* Styling only works when defined *inside* the marquee. */
span.jungle-friend:focus::after,
span.jungle-friend:hover::after {
  content: ' HAKUNA MATATA';
  font-size: 0.618em;
}
</style>
<span class="jungle-friend">🐗</span>
</marquee>



Lire les [papiers récents](https://scholar.google.fr/scholar?as_ylo=2019&q=%22liquid+democracy%22&hl=en&as_sdt=0,5&as_vis=1) (2019).

!! Une alternative libre à Google Scholar serait appréciée, si vous en [connaissez une…](éditer)
