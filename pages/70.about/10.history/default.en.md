---
title: History
slug: history
---

## See the [Federated Wiki](https://liquiddemocracy.org/view/liquid-democracy).

<marquee>
🐗
</marquee>





### Overview of the scientific literature

Amount of published papers (and some books) containing `"liquid democracy"` in that order. 

Source: [Google Scholar](https://scholar.google.fr/scholar?q=%22liquid+democracy%22&hl=en&as_sdt=0%2C5&as_vis=1)
⋅
Raw Data: [CSV](./amounts-of-papers-and-books-about-liquid-democracy.csv)


![Timeseries of the total amount of papers mentioning liquid democracy](timeseries-liquid-democracy-papers-through-years.en.svg){.center}


---

Here are the [most recent (2019) papers](https://scholar.google.fr/scholar?as_ylo=2019&q=%22liquid+democracy%22&hl=en&as_sdt=0,5&as_vis=1) about it.

!! Libre alternatives to Google Scholar would be appreciated, if you [know of some](edit).
