---
title: Bienvenue
slug: accueil
body_classes: title-center
---

_Une démocratie plus intelligente et moins corruptible_ ~ avec [l'assemblée liquide](https://assemblee-liquide.fr) 🏊

---

[Construisons notre assemblée](../chantiers) et portons-la dans l'arène politique


## [À Propos de ce Wiki](../a-propos)

Ce wiki est dédié à la [_démocratie liquide_](../bibliotheque)
et à la mise en place d'une _assemblée liquide_
en france et ailleurs.

Ce wiki est sous domaine public (CC0).
Vous êtes chaleureusement invité⋅es à l'enrichir ; nous cherchons l'exhaustivité.



## [L'Assemblée Liquide](https://assemblee-liquide.fr)

Assemblée connectée pour voter les propositions de loi et désigner ses délégués de confiance.

- **SIÉGEZ** ! Chaque citoyen a son siège, personnel et permanent.
- **VOTEZ** directement sur les propositions de loi qui vous tiennent à cœur.
- **DÉLÉGUEZ** aux citoyens en qui vous avez confiance
  … et révoquez-les à tout instant s'ils la perdent.


### Comment l'obtenir ?

- Aux élections traditionnelles,
  **ÉLISEZ** celles et ceux qui portent notre vision et nos délibérations, les « candidats liquides ».
- […](éditer)


<marquee>🏊</marquee>
