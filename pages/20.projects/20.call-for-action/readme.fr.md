---
title: 'Appel aux Maires'
slug: appel-aux-maires
body_classes: title-center
---

# Appel aux Maires

L'**appel aux maires** pour mener l'expérience de l'assemblée liquide avec des mairies pilotes, et le plan d'action pour les  municipales 2020,
- Offrir un accompagnement bénévole pour la mise en place d'une assemblée de délibération citoyenne locale ouverte à tous les habitants

!!!! Ceci est un wiki :)