---
title: Application web
slug: app
---

## Étude des solutions logicielles
Lisez le [survol des solutions logicielles](survol-des-apps-de-democratie-liquide).



## Les agendas législatifs (Scraper et API)

### Assemblée nationale

- https://github.com/direct-assemblee/DirectAssemblee-scraper
- https://framagit.org/parlement-ouvert/deliasse-api

