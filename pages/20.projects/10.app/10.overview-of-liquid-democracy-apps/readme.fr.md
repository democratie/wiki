---
title: Survol
slug: survol-des-apps-de-democratie-liquide
---

# Solutions logicielles

_La démocratie liquide, comment va-t-on faire en pratique ?_

Il existe plusieurs variantes de solutions logicielles.



## 🗳 Totalement centralisées

Une seule instance de confiance.


```mermaid

graph LR

Citoyen((Citoyen))
Serveur[Assemblée Liquide]
Resultat((Délibération))

Citoyen --> |s'authentifie| Serveur
Citoyen --> |vote sur| Serveur
Citoyen --> |délègue sur| Serveur
Serveur --> |publie| Resultat
Citoyen --> |consulte| Resultat

```


Solutions libres connues:

- [Congressus](https://github.com/PartiPirate/congressus)
- [liquid.us](https://liquid.us) _seule la moitié du code est libre_
- [adhocracy4](https://github.com/liqd/a4-product)
- [DemocracyOs](https://github.com/DemocracyOS/democracyos)
- [proxyfor.me](https://github.com/metamerman/proxyfor.me)
- [#MaVoix](https://www.mavoix.info/) _source?_
- [jaune.io](https://jaune.io/) _source?_
- … (que vive la polyphonie 🎷)

> **Idées de Dissertations**:
> _Un serveur informatique de souveraineté nationale est-il suffisament sécurisé si…_
> 
> - … il est géré par un collectif à but lucratif ?
> - … il est géré par un collectif à but non-lucratif ?
> - … il est géré par l'armée, comme le vote papier ?
> - … il ne fonctionne pas qu'avec des logiciels libres ?
> - … il ne fonctionne pas qu'avec du matériel libre ?

***

## 👭 Centralisées avec tierce authentification

Séparation des bureaux:

- Le premier bureau vérifie votre _identité citoyenne_ et votre _authenticité_ : c'est l'**authentification**
- Le second bureau collecte vos votes et délégations sans connaître votre nom et les publie anonymement

```mermaid

graph LR

Citoyen((Citoyen))
Resultat((Délibération))
BureauB[Second Bureau]
BureauA[Premier Bureau]
Proof(Preuve d'authenticité)
TokenA(Identifiant Anonyme A)
TokenB(Identifiant Anonyme B)


Citoyen --> |consulte| Resultat
BureauB --> |ne connaît pas| Citoyen
BureauA --> |ne connaît pas| TokenB
BureauB --> |génère| TokenB
Citoyen -.- TokenB
Citoyen --> |s'authentifie| BureauA
TokenB --> |vote sur| BureauB
TokenB --> |délègue sur| BureauB
Citoyen -.- TokenA
BureauA --> |génère| Proof
BureauA --> |génère| TokenA
Proof --> |est transmise à| BureauB
Proof -.- TokenA
TokenA --> |s'authentifie| BureauB

BureauB --> |publie| Resultat
```

Solutions connues:

- _néant_


> Sujets intéressants de dissertation : **l'interdiction de divulguer des identifiants anonymes, …**
> 
> - … point.
> - … sans consentement
> - … y compris les siens.
> 
> 👎 _Restriction des libertés_
>
> 👍 _Protection contre la coercition_



## ✨ Décentralisées via Solid

[Solid](https://solid.mit.edu/) est un protocole anti-silo et décentralisant,
créé par la personne qui a écrit la toute première page web,
le temps d'une gestation après la subversion du W3C par les lobbies des GAFAM,
qui ont manoeuvré pour instaurer la tyrannie de la majorité
là où n'avait vécu que le consensus pendant plus de vingt ans.
([source](https://boingboing.net/2017/09/18/antifeatures-for-all.html))

```mermaid

graph LR

Citoyen((Citoyen))
Resultat((Délibération))
Pod[Pod Privé]
Bureau[Assemblée Liquide]

Citoyen --> |s'authentifie| Bureau
Citoyen --> |vote sur| Pod
Citoyen --> |délègue sur| Pod
Pod -.- |SOLID| Bureau

Bureau --> |publie| Resultat
Citoyen --> |consulte| Resultat
```


Solutions connues:

- _néant_


> Peut aussi exister en version avec tierce authentification



## ⛓ Décentralisées via chaîne de blocs

Les _dApps_, les _applications décentralisées_, sont des merveilles d'horlogerie.

```mermaid

graph LR

Citoyen((Citoyen))
Resultat((Délibération))
Chaine[Chaîne de Blocs]
BureauA[Assemblée Liquide]

Citoyen --> |s'authentifie| BureauA
Citoyen --> |vote sur| BureauA
Citoyen --> |délègue sur| BureauA
BureauA --> |s'authentifie| Chaine
BureauA --> |vote sur| Chaine
BureauA --> |délègue sur| Chaine

Chaine --> |publie| Resultat
Citoyen --> |consulte| Resultat
```

Solutions connues:

- [Democracy.Earth](https://democracy.earth)


---

> Vous avez lu jusqu'en bas ! Vous gagnez **43** points d'expérience.
>
> N'hésitez pas à ajouter d'autres architectures et solutions,
> nous cherchons l'exhaustivité !

