---
title: "Nos Grands Chantiers"
menu: Chantiers
slug: chantiers
---

### Nos grands chantiers

![Puzzle Sous les Pavés la Plage Liquide](puzzle-rocks-floor-420.png){.center}

1. L'[**application web**](app) de [l'**Assemblée liquide**](https://assemblee-liquide.fr/),
    - Ouvrir et développer le service web,
        - Fédérer les services complémentaires de démocratie participative sur l'interface utilisateur :
          [_DirectAssemblée_](https://direct-assemblee.org),
          [_RegardsCitoyens_](https://www.regardscitoyens.org),
          [_Communecter_](https://www.communecter.org),
          [_Politizr_](https://www.politizr.com),
          [_Parlement & Citoyens_](https://parlement-et-citoyens.fr),
          [_Sénat Citoyen_](http://www.senatcitoyen.fr),
          [_Nexxo_](https://nexxociety.co),
          [_Logora_](https://logora.fr),
          [etc.](edit)
    - Organiser le secrétariat pour pérenniser le développement de l'assemblée
        - Se rassembler à l'occasion d'une _Cérémonie inaugurale de l'Assemblée liquide_
    
2. L'[**appel aux maires**](appel-aux-maires) pour mener l'expérience de l'assemblée liquide avec des mairies pilotes, et le plan d'action pour les municipales 2020,
    - Offrir un accompagnement bénévole pour la mise en place d'une assemblée de délibération citoyenne locale ouverte à tous les habitants
 
3. La **contagion** culturelle.  Citoyenne, académique, médiatique, politique, etc.
    - Propager notre message et construire le dialogue _#liquide_
    - Former l'_#AllianceMontesquieu_ pour la séparation des pouvoirs

4. Le **lobbying** avec les civictechs pour l'ouverture du service FranceConnect, afin de créer de la valeur ajoutée aux offres de civictech par la mise à disposition d'un service anonymisé de validation de compte utilisateur. 
    - Campagne "_#FranceConnectavecCiviTech_" à mener avec le collectif [DémocratieOuverte](https://democratieouverte.org/), [CodeForFrance](http://codefor.fr), [Parlement & citoyens](https://parlement-et-citoyens.fr/), etc.



